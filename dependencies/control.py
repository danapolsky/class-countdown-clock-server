"""
Control functions and constants
"""

# Import required datetime functions
from datetime import timedelta

# Import required arrow functions
import arrow
from arrow import now as arrow_now

# Define constants
TIME_STR_FORMAT = 'HH:mm:ss' # Hour, Minute, Second
START_MSG = 'startup'
STOP_MSG = 'stop'

def start_time_file_setup(filename:str, hour:int, minute:int=0, second:int=0):
    """Set up the start time file."""
    
    # Create a time object
    startTime = arrow.utcnow().replace(hour=hour, minute=minute, second=second)
    
    # Format the time object into a string
    startTimeString = startTime.format(TIME_STR_FORMAT)
    
    # For testing, print the string
    print("Formatted Time String:", startTimeString)
    
    # Write the startTimeString to the file
    with open(filename, "w") as fileObject:
        fileObject.write(startTimeString)


class StartTimeManager():
    """Get the start time and the time left until the start time."""
    
    def __init__(self, startTimeFile:str, standardDelay:float):
        self.__startTimeFile = startTimeFile
        self.standardDelay=standardDelay


    def __get_start_time(self) -> arrow.Arrow:
        """Get the start time from the file, then format it to tomorrow."""
        
        # Get the time string from its file
        with open(self.__startTimeFile, 'r') as fileObject:
            timeString = fileObject.readline()
        
        # Parse the time from the string
        startTime = arrow.get(timeString, TIME_STR_FORMAT)
        
        # Try to get tomorrow's date
        try:
            tomorrowDate = self.__tomorrow
        except AttributeError:
            raise AttributeError('Set up tomorrow with the "set_tomorrow(today)" method first.')
        else:
            # Replace the hour, minute, and second of tomorrow with the ones from the file
            startDatetime = tomorrowDate.replace(hour=startTime.hour, minute=startTime.minute, second=startTime.second)
            
            # Return the datetime for the correct time tomorrow
            return startDatetime


    def set_tomorrow(self) -> None:
        """Set up tomorrow's date based on today's date."""
        self.__tomorrow = arrow_now().shift(days=1)


    def get_sec_until_start(self) -> int:
        """Get the number of seconds left until the start time."""
        
        # Get the startDateTime from file
        startDateTime = self.__get_start_time()
        
        # Calculate the hours left until the start datetime
        timeLeft = (startDateTime - arrow_now())
        
        # If there is more than 1 hour left until the start time, return the standard delay
        if round(timeLeft / timedelta(hours=1)) > 1:
            return self.standardDelay

        # Else, return the actual time left in seconds
        else:
            return round(timeLeft / timedelta(seconds=1))


    def stop_msg(self) -> str:
        """Return the stop message to send to the subscribers and publishers so they stop their activity."""
        return STOP_MSG


    def start_msg(self) -> str:
        """Return the start message to send to the subscribers and publishers so they resume activity."""
        return START_MSG


def should_run(shouldRunFile:str) -> bool:
    """Determine if the program should run based on whether 1 or 0 is stored in a file."""
    with open(shouldRunFile, "r") as fileObject:
        # Convert the string to an integer to a boolean
        shouldRun = bool(int(fileObject.readline()))
    return shouldRun
