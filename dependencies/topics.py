"""
Constant topic definitions
"""

# Topics server sends to clocks
ASSIGNMENTS_TOPIC = 'assignments'
LOGIN_TOPIC = 'login'

# Topic server receives from clocks
CLOCKS_TOPIC = 'clocks'

# Topic server sends to server
START_STOP_TOPIC = 'startstop'