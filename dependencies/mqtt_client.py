"""
Base class for communicating with the MQTT server
"""

# To communicate with the mqtt broker
from paho.mqtt.client import Client

# For logging
from logging import basicConfig, getLogger, DEBUG

# Import the start-stop topic
from dependencies.topics import START_STOP_TOPIC

# Import the start and stop messages
from dependencies.control import START_MSG, STOP_MSG

# Base MQTT Client class
class MQTTClient(Client):
    """Client class to send data to the clocks."""

    def __init__(self, client_id:str, mqttDict:dict):
        super().__init__(client_id)
        
        self.__currentlyRunning = True # Set the currently running flag to True by default
        
        self.__host_address = mqttDict['host_address']
        self.__startStopTopic = START_STOP_TOPIC

        # Set the username and password
        self.username_pw_set(mqttDict['user'], mqttDict['password'])
        
        # Configure the logging
        basicConfig(filename=fr'logs/{client_id}.log', level=DEBUG, format='%(asctime)s:%(levelname)s:%(message)s')
        logger = getLogger()
        
        # Enable logging
        self.enable_logger(logger)
        
        # Add a callback for the start-stop topic
        self.message_callback_add(self.__startStopTopic, self.on_start_stop_msg)

    def get_start_stop_topic(self) -> str:
        """Get the start-stop topic."""
        return self.__startStopTopic

    def on_connect(self, client, userdata, flags, rc):
        """Called when the client connects to the MQTT Server. Subscribe to the start-stop topic."""
        
        # Print that the client has connected
        print(f"{self._client_id} connected with result code {rc}.")
        
        # Subscribe to the start-stop topic
        client.subscribe(self.__startStopTopic)

 
    def on_start_stop_msg(self, client, userdata, message):
        """Callback to when a start or stop message is received from the server."""
        
        # Decode the message
        stateMessage = message.payload.decode('ascii')
        
        # If the message is to stop, set the currently running flag to False
        if stateMessage == STOP_MSG:
            self.__currentlyRunning = False
        
        # Else if the message is to start, set the currently running flag to True
        elif stateMessage == START_MSG:
            self.__currentlyRunning = True


    @property
    def currentlyRunning(self) -> bool:
        """Property to determine if the program should run."""
        return self.__currentlyRunning


    def on_publish(self, client, userdata, mid):
        print("mid: " + str(mid))


    def connect_async(self, host, port=1883, keepalive=60, bind_address="", bind_port=0, clean_start=..., properties=None):
        return super().connect_async(host, port, keepalive, bind_address, bind_port, clean_start, properties)


    def get_host(self) -> str:
        """Get the host address to connect to. This method should be used when connecting.""" 
        return self.__host_address