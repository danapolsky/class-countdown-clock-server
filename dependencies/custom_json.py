# JSON reading and writing

from json import dump, load
from collections import UserDict

def json_read(filename:str) -> dict:
    with open(filename, 'r') as fileObject:
        dictionary = load(fileObject)
    return dictionary

def json_write(filename:str, json_object:dict):
    with open(filename, 'w') as fileObject:
        dump(json_object, fileObject, indent=4)

# Dictionary from a JSON file
class jsonDict(UserDict):
    def __init__(self, filename:str, /, **kwargs):
        self.filename = filename
        super().__init__(self.__json_read(), **kwargs)

    def __json_read(self) -> dict:
        with open(self.filename, 'r') as fileObject:
            dictionary = load(fileObject)
        return dictionary

    def __json_write(self):
        with open(self.filename, 'w') as fileObject:
            dump(self.data, fileObject, indent=4)

    def setdefault(self, key, value):
        """Redefine setdefault to read from the file, set the default, write to the file, and return the value."""
        self.update(self.__json_read())
        returnedVal = super().setdefault(key, value)
        self.__json_write()
        return returnedVal