"""
Constant file location definitions
"""

# Data files
LOGIN_FILE = r'Data/login.json'
ASSIGNMENTS_FILE = r'Data/assignments.json'
SCHEDULES_FILE = r'Data/schedules.json'
NAMES_FILE = r'Data/names.json'

# Control files
START_TIME_FILE = r'Control/start_time.control'