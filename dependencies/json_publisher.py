"""
Reads from a json file and sends the json data to the clocks
"""

# Import the MQTT Client class
from dependencies.mqtt_client import MQTTClient

# Import json_read to get data from JSON files
from dependencies.custom_json import json_read

# Import dumps from json to send the json file to the clocks
from json import dumps

# Import sleep
from time import sleep


class JSONPublisher(MQTTClient):
    """Sends json data to the clocks via the broker."""
    
    def __init__(self, loginFile:str, dataFile:str, client_id:str, topic:str, send_interval:float):
        self.__dataFile = dataFile
        self.__topic = topic
        self.__send_interval = send_interval

        # Initialize the MQTT Client
        super().__init__(client_id, json_read(loginFile)['mqtt'])
    
    def publish_json(self):
        """Publish the json data to the broker."""
        
        # Get the json data from the json file
        json_data = json_read(self.__dataFile)
        
        # Publish the json_data to the broker
        return self.publish(self.__topic, dumps(json_data), retain=True)
    
    def publish_loop(self):
        """Loop that sends the assignments to the broker."""

        # Wait until connected, then start the threaded loop
        self.connect(self.get_host())
        self.loop_start()

        print("Publishing start.")
        
        # Forever, publish the json data
        while True:
            
            # If the program should run, publish the json data
            if self.currentlyRunning:
                self.publish_json()
            
            # Sleep for send interval seconds
            sleep(self.__send_interval)