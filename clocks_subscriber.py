"""
Receive and parse messages from the clocks
"""

# Import the MQTT Client class for communication as well as json_read and jsonDict to get data to and from JSON files
from dependencies import MQTTClient, json_read, jsonDict

# Import the required file constants
from dependencies.files import LOGIN_FILE, ASSIGNMENTS_FILE

# Import all the topics
from dependencies.topics import ASSIGNMENTS_TOPIC, CLOCKS_TOPIC, LOGIN_TOPIC

class ClocksSubscriber(MQTTClient):
    """Receive messages from the clocks, update the assignments, and log the messages."""
    
    def __init__(self, loginFile:str, assignmentsFile:str):

        # Set up the assignments
        self.__assignments = jsonDict(assignmentsFile)
        
        # Initialize the MQTT Client
        super().__init__('ClocksSubscriber', json_read(loginFile)['mqtt'])
        
        # Add message callbacks for the different subtopics
        self.message_callback_add(CLOCKS_TOPIC+"/startup", self.on_startup_msg)
        
        self.message_callback_add(CLOCKS_TOPIC+"/reboot/"+ASSIGNMENTS_TOPIC, self.on_reboot_assignments_msg)
        self.message_callback_add(CLOCKS_TOPIC+"/reboot/"+LOGIN_TOPIC, self.on_reboot_login_msg)

    def on_connect(self, client, userdata, flags, rc):
        """Additionally subscribe to all the clocks topics when connected."""
        
        # Run the parent on_connect method
        super().on_connect(client, userdata, flags, rc)
        
        # Subscribe to all clocks topics
        client.subscribe(CLOCKS_TOPIC+"/#")


    def on_startup_msg(self, client, userdata, message):
        """Callback to when a startup message is received from a clock."""
        
        # Get the id of the clock from its message
        id = message.payload.decode('ascii')

        # Get the stored schedule assignment for the clock if it exists.
        # Else, set its assignment to the default schedule assignment
        assignment = self.__assignments.setdefault(id, self.__assignments['Default'])

        print('ID:', id, 'Assignment: ', assignment)


    def on_reboot_assignments_msg(self, client, userdata, message):
        """Callback to when a reboot assignments message is received from a clock."""
        
        # Get the id of the clock from its message
        id = message.payload.decode('ascii')

        print('ID:', id, 'Rebooting:', "assignments")


    def on_reboot_login_msg(self, client, userdata, message):
        """Callback to when a reboot login message is received from a clock."""

        # Get the id of the clock from its message
        id = message.payload.decode('ascii')

        print('ID:', id, 'Rebooting:', "login")
    
    
    def start(self):
        """Wait until connected to the broker, then start the loop to continue forever."""
        self.connect(self.get_host())
        self.loop_forever(retry_first_connection=True)


# When the code runs
if __name__ == '__main__':

    # Create a ClocksSubscriber object
    clocks_subscriber = ClocksSubscriber(LOGIN_FILE, ASSIGNMENTS_FILE)

    # Print that the clocks subscriber is setting up
    print('Clocks subscriber setting up.')

    # Run the subscriber forever
    clocks_subscriber.start()

    # Print that it is stopping for the day
    print('Stopping for today.')