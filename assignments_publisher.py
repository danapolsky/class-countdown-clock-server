"""
Reads from the assignments json file and sends them to the clocks
"""

# Import the JSON Publisher class for basic json publishing
from dependencies import JSONPublisher

# Import the required file constants
from dependencies.files import LOGIN_FILE, ASSIGNMENTS_FILE

# Import the required topic constant
from dependencies.topics import ASSIGNMENTS_TOPIC

# When the code runs
if __name__ == '__main__':

    # Create an JSON publisher object
    assignments_publisher = JSONPublisher(LOGIN_FILE, ASSIGNMENTS_FILE, "AssignmentsPublisher", ASSIGNMENTS_TOPIC, 10)

    # Print that the assignment publisher is setting up
    print('Assignment publisher is setting up.')
    
    # Start the publishing loop
    assignments_publisher.publish_loop()

    # Print that it is stopping for the day
    print('Stopping for today.')