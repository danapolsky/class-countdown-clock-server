"""
Reads from the login json file and sends them to the clocks
"""

# Import the JSON Publisher class for basic json publishing
from dependencies import JSONPublisher

# Import the required file constants
from dependencies.files import LOGIN_FILE

# Import the required topic constant
from dependencies.topics import LOGIN_TOPIC

# When the code runs
if __name__ == '__main__':

    # Create an JSON publisher object
    login_publisher = JSONPublisher(LOGIN_FILE, LOGIN_FILE, "LoginPublisher", LOGIN_TOPIC, 10)

    # Print that the login publisher is setting up
    print('Login publisher is setting up.')
    
    # Start the publishing loop
    login_publisher.publish_loop()

    # Print that it is stopping for the day
    print('Stopping for today.')
