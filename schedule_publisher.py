"""
Downloads, formats, and sends the schedules to the MQTT broker
"""

# From the asyncio module import the required functions and classes to run multiple tasks at once
from asyncio import create_task, gather, run as async_run, sleep as asleep

# Import the timedelta class from the datetime module for comparing the current time and the time class ends
from datetime import timedelta

# Import required modules for the iCal feed
from urllib.error import URLError, HTTPError
from urllib.request import urlopen
from ics import Calendar, Event
from arrow import now as arrow_now

# Import the MQTT Client class and json_read function to connect to the broker and to read from the json file
from dependencies import MQTTClient, json_read

# Import the required file constants
from dependencies.files import LOGIN_FILE, NAMES_FILE, SCHEDULES_FILE, START_TIME_FILE

# Import the StartTimeManager class
from dependencies.control import StartTimeManager

# Define the SchedulePublisher class
class SchedulePublisher(MQTTClient):
    """Get the schedules from blackbaud and update the clocks."""
    def __init__(self, loginFile:str, schedulesFile:str, nameDictionaryFile:str, startTimeFile:str, startTimeDelay:float):
        # Store the filenames in data attributes
        self.__schedulesFile, self.__nameDictionaryFile = schedulesFile, nameDictionaryFile

        # Get the schedules
        self.__schedules = json_read(self.__schedulesFile)

        # Get and format the name dictionary
        self.__name_dictionary = self.__name_dictionary_setup()
        
        # Initialize the MQTT Client
        super().__init__('SchedulePublisher', json_read(loginFile)['mqtt'])
        
        # Create a StartTimeManager object
        self.stm = StartTimeManager(startTimeFile, startTimeDelay)


    # Define the __name_dictionary_setup method to get a dictionary with all the event names and their displayed values
    def __name_dictionary_setup(self) -> dict:
        """Get and format the name dictionary from its file."""

        # Get the names from their file
        names = json_read(self.__nameDictionaryFile)

        # Add the unique names to the name dictionary
        name_dictionary = names['Unique']

        # For multi_name, multi_list in the multiple dictionary, add the names to the name dictionary
        for multi_name, multi_list in names['Multiple'].items():
            name_dictionary.update(dict.fromkeys(multi_list,multi_name))

        # Return the name dictionary
        return name_dictionary


    # Define the publish method to publish text to the MQTT Broker
    async def publish(self, text:str, topic:str, delay:float=0, retain:bool=False):
        print(topic, '-', text + '\n')
        
        # Publish to the broker
        super().publish(topic, text, retain=retain)

        # Sleep for delay seconds
        await asleep(delay)


    async def update_calendars(self):
        """Get the calendars, get the messages, and update the schedules dictionary."""
        
        # Get the calendars and messages using the urls of the schedules
        calendar_message_list = await gather(*[create_task(self.__get_calendar(values['topic'], values['url'])) for values in self.__schedules.values()])

        # Add the calendars and messages to the schedule dictionaries
        [self.__schedules[schedule].update({'calendar':calendar_message[0], 'message':calendar_message[1]}) for schedule, calendar_message in zip(self.__schedules.keys(), calendar_message_list)]


    async def get_send_tasks(self):
        """Create an async task for each schedule."""
        return [create_task(self.__send_current_event(values['topic'], values['calendar'], values['message'])) for values in self.__schedules.values()]


    # Define the run_tasks method to run the tasks
    async def run_tasks(self):
        """Run the asyncio tasks."""

        # Print that the schedule is setting up
        print('Schedule is setting up.')

        # Connect to the broker and start the loop in a thread
        self.connect_async(self.get_host())
        self.loop_start()

        # Update the calendars
        await self.update_calendars()

        # Run the send tasks
        tasks = await gather(*await self.get_send_tasks())


    # Define the __get_calendar method to get the iCal data from Blackbaud
    async def __get_calendar(self, topic:str, url:str):
        """Format the iCal data into a Calendar object."""
        error_format = '{}-{}: {}'

        attempt = 0
        # If the attempt number is less than 10, try to get the calendar
        while attempt < 10:

            # Try to get the calendar
            try:

                # Create an empty Calendar object to store the events
                calendar = Calendar()

                # Set index to 0 by default
                index = 0

                # Download the iCal data
                iCalData = urlopen(url)

                # Display that the iCal data is being read
                print('rEAd')
                
                # Read the iCal data
                iCalData = iCalData.read()

                # Display that the iCal data is being decoded
                print('dECodE')

                # Decode the iCal data
                iCalData = iCalData.decode('iso-8859-1')

                # Display that the iCal data is being sliced
                print('SLICE')

                # Get the settings and events for today
                settings, events = await gather(self.__get_settings(iCalData), self.__find_today(iCalData))

                # Combine the settings and events together
                iCalData = settings+'\n'+events

                # Display that the iCal data is being turned into an Calendar object
                print('obJECt')

                # Create a Calendar object with the iCal data
                iCalData = Calendar(imports=iCalData)

                # Display that today's event are being found
                print('todAy')

                # Get today's events from the iCal data
                iCalData = iCalData.timeline.today(True)

                # For index, event in enumerate of all the non-all day events happening today
                for index, event in enumerate(iCalData):

                    # Print the number of events formatted so far
                    print('Formatted Events:', index)

                    # Display the number of events formatted so far on the screen
                    print('FoUnd. ' + await self.__format_minutes(index))
                    
                    # Format the event's name
                    event.name = await self.__format_name(event.name)

                    # Add the event to the event calendar
                    calendar.events.add(event)
                
                # Display the number of events formatted so far on the screen
                print('FoUnd. ' + await self.__format_minutes(index))
                
                # Print the title of the event info
                print('Event Data:')

                # Print the event names
                [print(event.name) for event in calendar.timeline.__iter__()]

            # Catch a HTTPError or RuntimeError, which both indicate file downloading errors
            except (HTTPError, RuntimeError):

                # Printdownloading exception
                print(error_format.format(topic, attempt, 'Err. fILE (error downloading calendar file)'))

            # Catch a URLError or OSError, which both indicate network connection errors
            except (URLError, OSError):

                # Print the network connection exception
                print(error_format.format(topic, attempt, 'Err. nEt (internet connection not available)'))

            # Else, if no error occurred, return the calendar and off as the message
            else:
                return calendar, 'off'
            
            # Finally, add 1 to attempt and sleep for 1 second
            finally:
                attempt += 1
                await asleep(1)

        # Else, if the calendar could not be gotten, return the empty calendar and an error as the message
        else:
            return calendar, 'Err. CAL'


    # Define the __send_current_event method to determine the current event and send it to the nodes
    async def __send_current_event(self, topic:str, calendar:Calendar, message:str):
        """Determine the current event in the schedule and send it to the nodes."""

        # Publish the topic as the retained message to display when a node has not yet received a message
        await self.publish(topic, topic, retain=True)


        # Forever, until an error occurs which signals there are no more events in the day
        while True:

            # Try to get an event currently happening and format the name of the event
            try:
                event = next(calendar.timeline.now())
                event.name = await self.__format_description(event.name)

            # Catch a StopIteration exception if there are no events currently happening
            except StopIteration:

                # Try to get the next event in the calendar
                try:
                    nextEvent = next(calendar.timeline.start_after(arrow_now()))

                # Catch a StopIteration exception if there are no future events today
                except StopIteration:

                    # Publish closed and wait 5 seconds
                    await self.publish(':CLOSEd:', topic, delay=5)

                    # Publish the final message
                    await self.publish(message, topic)

                    # Return the topic to indicate that the schedule is done
                    return topic

                # Else, create an event that ends when the next event starts and has 'to' in the name
                else:

                    # Format the name of the event
                    eventName = await self.__format_description('to ' + nextEvent.name)

                    # Create the to-event event
                    event = Event(name=eventName, begin=arrow_now().shift(minutes=-2), end=nextEvent.begin)

            # Display the event name and time left in the event until the event is over
            await self.__publish_event(event, topic)


    # Define the __publish_event method to publish information about the current event
    async def __publish_event(self, event:Event, topic:str):
        # Set the done flag to False by default
        done = False

        # Get the event's name
        name = event.name

        # While the event is not done
        while not(done):

            # Calculate the time left until the end of the event
            timeLeft = (event.end - arrow_now())

            # By default, set time to minutes rounded to the nearest minute and formatted, and delay to 0.25
            time = await self.__format_minutes(round(timeLeft / timedelta(minutes=1)))
            delay = 0.25

            # If the time left is greater than 99 minutes, then display 99 minutes left and delay by 1 minute
            if timeLeft > timedelta(minutes=99):

                # Set time to 99
                time = '99'

                # Set the delay to 1 minute
                delay = 60

            # Else if the time left is greater than 1 minute and 30 seconds, then display minutes left and delay by 30 seconds
            elif timeLeft > timedelta(minutes=1, seconds=30):

                # Set the delay to 30 seconds
                delay = 30

            # Else if the time left is greater than 1 minute, then display minutes left and delay by 0.25 seconds
            elif timeLeft > timedelta(minutes=1):

                # Do nothing since the time and delay is already correct 
                pass

            # Else if the time left is greater than 0 seconds, then display seconds left and delay by 0.25 seconds
            elif timeLeft > timedelta(seconds=0):

                # Convert the time left to seconds, round to the nearest second, and format it
                time = await self.__format_minutes(round(timeLeft / timedelta(seconds=1)))

            # Else, if the time left is less than or equal to 0 seconds
            # then publish minutes left (which should be 0), delay by 0.25 seconds,
            # and set the done flag to True
            else:

                # Set the done flag to True
                done = True

            # Publish the event name and time left on the screen
            await self.publish(name+time, topic=topic, delay=delay)


    # Define the __format_name method to format the event names to be displayed on the 7 segment display
    async def __format_name(self, name:str):
        # Lower name to make searching for phrases easier
        name = name.lower()

        # Set the notFound flag to True
        notFound = True

        # For eventName in the dictionary of event names
        for eventName in self.__name_dictionary:
            # If eventName is in the name, then format the name
            # according to the eventName's value and set the flag to True
            if eventName in name:

                # Format the name according to the key's value
                formatName = self.__name_dictionary.get(eventName)

                # Set the notFound flag to False
                notFound = False

        # If the name has not been found and the event is a course, then format it accordingly
        if notFound and name[-1].rstrip().endswith(')'):

            # Store the index of the open parenthesis that has the class period in it in openIndex
            openIndex = name.rindex('(')

            # Store the class period in period
            period = name[openIndex:].rstrip(' ').strip('(').strip(')')

            # If any letters need to be uppercase, upper period
            if period[0] in ('a', 'c', 'e', 'f', 'g', 'h'):
                # Upper period
                period = period.upper()

            # Assemble the text to be displayed and store it in formatName
            formatName = period + '.'

        # Else if the name is still not found, add a '.' to the original name
        elif notFound:

            # Add a decimal to the original name
            formatName = name + '.'

        # Return formatName
        return formatName


    # Define the __format_minutes method to format the minutes to be displayed on the 7 segment display
    async def __format_minutes(self, minutes:int, formatThreeDigits=False):
        # Store the original minutes as an integer
        intMin = int(minutes)

        # Ensure minutes is a string
        minutes = str(minutes)

        # If minutes is less than 10, then convert it to a string with a 0 in front of it
        if intMin < 10:

            # Format minutes with a 0
            minutes = '0' + minutes

        # If formatThreeDigits is True
        if formatThreeDigits:

            # If minutes is less than 100, then add another 0 in front of it
            if intMin < 100:

                # Format minutes with a 0
                minutes = '0' + minutes

            # Else if minutes is greater than 999, then set minutes to 999
            elif intMin > 999:

                # Set minutes to 999
                minutes = '999'

        # Else if minutes is greater than 99 (and formatThreeDigits is not True), then set minutes to 99
        elif intMin > 99:

            # Set minutes to 99
            minutes = '99'

        # If minutes is a negative integer, then instead format it as 0
        if '-' in minutes:

            # Slice out the negative number and add another 0
            minutes = '0' + minutes[:minutes.index('-')]

        # Return minutes converted to a string
        return str(minutes)


    # Define the __format_description method to format the description to be 6 characters long
    async def __format_description(self, description:str):
        # If description is less than 6 characters, add spaces
        if len(description.replace('.', '')) < 6:

            # While the length of the description is less than 6 characters, add a space to description
            while len(description.replace('.', '')) < 6:
                # Add a space to the end of description
                description += ' '

        # Else if description is more than 6 characters, slice out characters
        elif len(description.replace('.', '')) > 6:

            # While the length of the description is greater than 6 characters, slice out the last character of description
            while len(description.replace('.', '')) > 6:
                # Slice out the last character of description
                description = description[:-1]

            # Add a '.' to the end of description
            description += '.'

        # Return description
        return description


    async def __get_settings(self, data:str):
        """Get the settings of an iCal calendar from a decoded string."""
        # Setup
        settingsStart = 'BEGIN:VCALENDAR'
        settingsEnd = 'END:VTIMEZONE'

        # Slice settings
        settings = data[data.index(settingsStart): data.index(settingsEnd)+14] # Offset by 14 to account for the length of the settingsEnd plus 1
        return settings


    async def __find_today(self, data:str):
        """Get the events that are happening today from a decoded string of an iCal calendar."""
        # Setup
        eventBegin, eventEnd = 'BEGIN:VEVENT', 'END:VEVENT'
        today = 'o:' + arrow_now().format('YYYYMMDD') # Add 'o:' to ensure it looks for 'DTEND;TZID=America\Chicago:[Today's Date]' instead of 'DTSTAMP:[Today's Date]'

        # Find start of first event
        startDateIndex = data.find(today)
        startEventIndex = data.rfind(eventBegin, 0, startDateIndex)

        # First end of last event
        endDateIndex = data.rfind(today, startDateIndex)
        endEventIndex = data.find(eventEnd, endDateIndex)

        # Slice events
        events = data[startEventIndex: endEventIndex+10] # Offset by 10 to account for the length of the eventEnd
        return events


    async def wait_until_start_time(self):
        """Wait until the schedule should be found and published again, notifying the server along the way."""
        
        # Set up tomorrow's date as today plus 1 day
        self.stm.set_tomorrow()
        
        # Set the initial delay to the standard delay
        delay = self.stm.standardDelay
        
        # Once the delay is not the standard delay, send the stop topic, wait the nonstandard delay time, then exit the while loop
        while delay == self.stm.standardDelay:
            
            # Get the delay again
            delay = self.stm.get_sec_until_start()
            
            # Send the stop message in the start-stop topic and sleep for the delay
            await self.publish(self.stm.stop_msg(), self.get_start_stop_topic(), delay, retain=True)
            
        else:
            
            # Send the start message with no delay
            await self.publish(self.stm.start_msg(), self.get_start_stop_topic(), retain=True)


# When the code runs
if __name__ == '__main__':
    
    # Create a SchedulePublisher object
    schedule = SchedulePublisher(LOGIN_FILE, SCHEDULES_FILE, NAMES_FILE, START_TIME_FILE, startTimeDelay=3600)

    # Forever
    while True:

        # Run the schedule publishing tasks
        async_run(schedule.run_tasks())
        
        # Print that it is stopping for the day
        print('Stopping for today.')
        
        # Wait until the start time
        async_run(schedule.wait_until_start_time())
