# Class-Countdown-Clock-Server

The Server that sends data to the nodes using the MQTT Broker

## Dependencies

```bash
paho-mqtt
ics
arrow
```

## Setup of Linux MQTT Server

1. Download and install Ubuntu
2. Install the software update if there is one
3. Enable SSH to make setting up easier by accessing its terminal from PuTTY on another computer: [tutorial](https://www.cyberciti.biz/faq/ubuntu-linux-install-openssh-server/)
4. Install the MQTT Server and testing clients:

    ```bash
    sudo apt-get update
    sudo apt-get install mosquitto
    sudo apt-get install mosquitto-clients
    ```

5. Test the mosquitto server locally: [tutorial](https://www.vultr.com/docs/how-to-install-mosquitto-mqtt-broker-server-on-ubuntu-16-04/)
6. Create a username and password for clocks to use to connect to the broker:

    ```bash
    sudo mosquitto_passwd -c /etc/mosquitto/passwd <usename>
    Password: <password>
    ```

7. Create a config file for the MQTT Server:

    ```bash
    sudo nano /etc/mosquitto/conf.d/default.conf
    ```

8. Add the following to the config file to make it persistent, and to set up the password:

    ```bash
    allow_anonymous false
    persistence true

    listener 1883
    password_file /etc/mosquitto/passwd
    ```

9. Restart the MQTT server to apply the changes

    ```bash
    sudo systemctl restart mosquitto
    ```

10. Set the server to have a static IP address: [tutorial](https://linuxconfig.org/how-to-configure-static-ip-address-on-ubuntu-18-10-cosmic-cuttlefish-linux#:~:text=Ubuntu%20Desktop,-The%20simplest%20approach&text=Click%20on%20the%20top%20right,netmask%2C%20gateway%20and%20DNS%20settings)

    ```bash
    XXX.XXX.XXX.YYY
    255.255.255.0
    XXX.XXX.XXX.1
    ```

## Setup of Code

### Python Setup

Set up a Python Virtual Environment

```bash
sudo apt install python3-pip
pip install virtualenv

virtualenv_name="ccc-env"
virtualenv -p /usr/bin/python3 $virtualenv_name
source $virtualenv_name/bin/activate

pip install paho-mqtt &&
pip install ics &&
pip install arrow
deactivate
```

### Install git and clone the repository

```bash
sudo apt install git
git clone https://gitlab.com/danapolsky/class-countdown-clock.git
```

### Start on reboot

- [tutorial for setup of the files via systemd](https://www.baeldung.com/linux/run-script-on-startup)
  - Also ```sudo chmod 744 main.sh```
- Make sure that ```ccc_mqtt.service``` has the correct working directory

```bash
cd $HOME
cd Class-Countdown-Clock-Server/
sudo cp ccc_mqtt.service /etc/systemd/system
cd
systemctl enable ccc_mqtt.service
```

### Start at specific time of day

- Follow the format instructions in `Control/start_time.control`

### Create default files

```/Data/assignnments.json```

```json
{
"Default": "A"
}
```

Create ```/logs``` directory

## Testing the server

```bash
systemctl start ccc_mqtt.service
systemctl status ccc_mqtt.service

sudo tail -n20 -f /var/log/mosquitto/mosquitto.log
```

## Enable, disable, and get the status of the program

Enable

```bash
systemctl enable ccc_mqtt.service
sudo reboot
```

Disable

```bash
systemctl disable ccc_mqtt.service
sudo reboot
```

Status

```bash
systemctl status ccc_mqtt.service
```
